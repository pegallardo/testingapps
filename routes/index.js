var express = require('express');
var router = express.Router();

var pg = require('pg');
var connectionString = "postgres://postgres:palta123@localhost/test";

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { heart: '<3' });
});

router.get('/data', function(req, res, next){
	var results = [];
	var client = new pg.Client(connectionString);
	client.connect();

	client.query("SELECT * FROM test;", (err, result) => {
		if (err)
			console.log(err.stack);
		else
			return res.send(result.rows[0]);
	});
	//res.render('testdb');
});

module.exports = router;
